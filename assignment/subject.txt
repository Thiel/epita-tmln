=============
Projet TMLN 1
=============

Sujet:
======
 * construire un outil en ligne de commande de correction orthographique rapide et stable en utilisant une distance de Damerau-Levenshtein
 * par groupe de 2 personnes
 * à rendre par email (zip ou tar.gz ou tar.bz2) à sylvain.utard@gmail.com avec balise [TEXTMINING] avant le 1 Août (exclu)

Contraintes:
=============
 * au minimum 3000 qps en distance = 0
 * au minimum 300 qps en distance = 1
 * au minimum 30 qps en distance = 2
 * le programme ne doit jamais consommer plus de 512M de RAM
 * le programme doit pouvoir tourner des mois sans le moindre crash ni memory leak.
 * Langages autorisés: C/C++ (boost autorisé, précisé la version dans le fichier README), Java, autre (avec justification + demande par email)
 * Autotools ou CMake si C/C++; Ant si Java; autre (avec justification + demande par email)

Livrable:
=========
 * Code source commenté avec Doxygen
 * README (explique le projet + répond aux questions ci-dessous)
 * AUTHORS (liste les login_l, format EPITA)
 * (configure et Makefile) ou build.xml
 * La compilation doit générer 2 binaires dans le répertoire racine:
   * TextMiningCompiler (2 arguments = fichiers des mots+freqs, dictionnaire compilé)
   * TextMiningApp (1 argument = dictionaire compilé)

Input:
======
 * Fichier des 3M de mots les plus fréquents extraits de pages web françaises (mots normalisés)
 
Implémentation:
===============
 * ref fournie
 * reproduire EXACTEMENT le comportement de la ref
 * correction par moulinette+diff strict (la sortie d'erreur n'est pas diffée)
 
Démo:
=====
./TextMiningCompiler /path/to/words.txt /path/to/dict.bin
echo "approx 0 test" | ./TextMiningApp /path/to/dict.bin
echo "approx 0 test\napprox 2 test" | ./TextMiningApp /path/to/dict.bin

Remarques:
==========
 * la réponse est au format JSON
 * les résultats doivent être triés par: distance (croissante), puis par fréquence (décroissante), puis par ordre lexicographique (croissant)

Questions:
==========
 1. Decrivez les choix de design de votre programme
 2.	Listez l’ensemble des tests effectués sur votre programme (en plus des units tests)
 3.	Avez-vous détecté des cas où la correction par distance ne fonctionnait pas (même avec une distance  élevée) ?
 4.	Quelle est la structure de données que vous avez implémentée dans votre projet, pourquoi ?
 5.	Proposez un réglage automatique de la distance pour un programme qui prend juste une chaîne de caractères en entrée, donner le processus d’évaluation ainsi que les résultats
 6.	Comment comptez vous améliorer les performances de votre programme
 7.	Que manque-t-il à votre correcteur orthographique pour qu’il soit à l’état de l’art ?
